import unittest
from subprocess import (
    Popen,
    PIPE,
)

__unittest = True


class Tester(unittest.TestCase):
    PROGRAM_PATH = './main'
    TIMEOUT_SECONDS = 5

    def setUp(self) -> None:
        self.existing_keys_and_outputs = {
            'Pichu': 'high Friendship',
            'Pikachu': 'use Thunder Stone, outside Alola',
            'Raichu': 'Last Evolution chart',
        }
        self.nonexistent_key_output = 'Not found'
        self.buffer_overflow_output = 'Too long'

    def test_existing_keys(self):
        for key, expected_output in self.existing_keys_and_outputs.items():
            output, code = self.launch_program(key)
            self.assertEqual(code, 0)
            self.assertEqual(output, expected_output)

    def test_nonexistent_keys(self):
        keys = ['sdjdkg', 'Lichu', 'AAAAAAAAAAAA']
        for key in keys:
            output, code = self.launch_program(key)
            self.assertEqual(code, 1)
            self.assertIn(output, self.nonexistent_key_output)

    def test_buffer_overflow(self):
        very_long_str = 'a' * 256
        output, code = self.launch_program(very_long_str)
        self.assertEqual(code, 1)
        self.assertIn(self.buffer_overflow_output, output)

    def test_full_buffer(self):
        key = 'a' * 255
        output, _ = self.launch_program(key)
        self.assertNotEquals(self.buffer_overflow_output, output)

    def launch_program(self, input: str) -> tuple[str, int]:
        popen = Popen(
            [self.PROGRAM_PATH],
            stdin=PIPE,
            stdout=PIPE,
            stderr=PIPE,
            text=True,
        )
        stdout, stderr = popen.communicate(
            input=input,
            timeout=self.TIMEOUT_SECONDS,
        )

        if stderr:
            return stderr, 1

        return stdout, 0


if __name__ == "__main__":
    unittest.main()
