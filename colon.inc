%define head_node 0  ; указатель на текщий последний элемент связного списка

%macro colon 2
%2:
dq head_node
db %1, 0 
%define head_node %2 ; обновляем указатель, теперь уже на текущий элемент
%endmacro