%include "words.inc"
%include "dict.inc"
%include "lib.inc"

%define SIZE 8
%define BUFF_SIZE 256
key_too_long_err: db "Too long", 0
dict_end_err: db "Not found", 0

section .bss
input_buff: resb BUFF_SIZE

section .text
global _start

_start:
    mov rdi, input_buff
    mov rsi, BUFF_SIZE
    call read_word
    test rax, rax           
    je .too_long
    push rdx
    mov rdi, rax            
    mov rsi, head_node       
    call find_word
    pop rdx            
    test rax, rax        
    je .not_found

    add rax, SIZE
    add rax, rdx
    inc rax

    mov rdi, rax
    push rsi
    mov rsi, 1      ; Ставим дескриптор STDOUT
    call print_string
    pop rsi

    .exit:          ; Конец,выходим
        xor rdi, rdi
        call exit

    .not_found:
        mov rdi, dict_end_err
        jmp .handle_error

    .too_long:
        mov rdi, key_too_long_err
    
    .handle_error:
        push rsi
        mov rsi, 2  ; ставим дескриптор STDERR
        call print_string
        pop rsi
        jmp .exit


