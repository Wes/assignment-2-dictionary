ASM=nasm
ASMFLAGS=-f elf64
LD=ld
COMPILE=$(ASM) $(ASMFLAGS)
SOURCE=$(wildcard *.asm)
INCLUDE=$(wildcard *.inc)
OBJECTS=$(SOURCE:.asm=.o)

all: $(OBJECTS)
	$(LD) -o main $^

%.o: %.asm $(INCLUDE)
	$(COMPILE) -o $@ $<

debug: main
	gdb main -q -ex "break _start" -ex "layout asm" -ex "layout regs"
	
clean:
	rm *.o
	rm main

test: all
	python3 test.py

.PHONY: clean